function createNewUser(firstName, lastName, birthday) {
    let newUser = {
        firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    getLogin: (function () {
        let login = this.firstName[0] + this.lastName;
        return login.toLowerCase();
    }),

    getAge: (function () {
        let user = new Date(birthday);
        let now = new Date();
        let ageMillis = now - user;
        let age = ageMillis / (1000 * 3600 * 365 * 24);
        return Math.floor(age);
    }),

    getPassword: (function () {
        let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
        return password;
    })
};
    return newUser;
}

let name = prompt('Enter your first name');
let surname = prompt('Enter your second name');
let dateArray = prompt('Enter your birthday', 'dd.mm.yyyy').split('.');
let date = new Date(Number(dateArray[2]), Number(dateArray[1]) - 1, Number(dateArray[0]));

let account = createNewUser(name, surname, date);

console.log(account.getLogin());
console.log(account.getAge());
console.log(account.getPassword());



